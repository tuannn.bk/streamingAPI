package com.tmovie.streaming.concurrency;

import com.tmovie.streaming.form.FilmRequestType;

public class BasePriority implements Comparable<BasePriority> {

	private long currentMils;
	private FilmRequestType requestType;
	private int tracking;
	
	public BasePriority(long currentMils, FilmRequestType requestType, int tracking) {
		super();
		this.currentMils = currentMils;
		this.requestType = requestType;
		this.tracking = tracking;
	}

	@Override
	public int compareTo(BasePriority o) {
		int requestTypeComparison = 
				Integer.compare(this.requestType.getValue(), o.requestType.getValue());
		if(requestTypeComparison != 0) return requestTypeComparison;
		
		int trackingComparison = Integer.compare(this.tracking, o.tracking);
		if(trackingComparison != 0) return trackingComparison;
		
		return Long.compare(this.currentMils, o.currentMils);
	}

}
