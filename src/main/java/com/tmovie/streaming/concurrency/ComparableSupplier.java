package com.tmovie.streaming.concurrency;

import java.util.function.Supplier;

public class ComparableSupplier<T> implements Supplier<T>, Comparable<ComparableSupplier<T>> {
	
	private BasePriority priority;
	private Supplier<T> supplier;

	public ComparableSupplier(Supplier<T> supplier, BasePriority priority) {
		this.supplier = supplier;
		this.priority = priority;
	}

	@Override
	public int compareTo(ComparableSupplier<T> o) {
		return this.priority.compareTo(o.priority);
	}

	@Override
	public T get() {
		return this.supplier.get();
	}

}
