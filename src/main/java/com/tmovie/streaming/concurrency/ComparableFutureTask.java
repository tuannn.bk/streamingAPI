package com.tmovie.streaming.concurrency;

import java.util.concurrent.Callable;
import java.util.concurrent.CompletableFuture;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.FutureTask;
import java.util.concurrent.RunnableFuture;

public class ComparableFutureTask<V> extends CompletableFuture<V> implements Comparable<ComparableFutureTask<V>>, RunnableFuture<V> {
	
	private BasePriority priority;
	private FutureTask<V> futureTask;
	
	public ComparableFutureTask(Callable<V> callable, BasePriority priority) {
		this.futureTask = new FutureTask<>(callable);
		this.priority = priority;
    }

	public ComparableFutureTask(Runnable runnable, V result, BasePriority priority) {
		this.futureTask = new FutureTask<>(runnable, result);
		this.priority = priority;
	}

	public BasePriority getPriority() {
		return priority;
	}

	@Override
	public int compareTo(ComparableFutureTask<V> o) {
		return this.priority.compareTo(o.priority);
	}

	@Override
	public void run() {
		this.futureTask.run();
		try {
			this.complete(this.futureTask.get());
		} catch (InterruptedException | ExecutionException e) {
			this.completeExceptionally(e);
		}
	}

}
