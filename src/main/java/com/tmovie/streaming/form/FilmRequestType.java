package com.tmovie.streaming.form;

public enum FilmRequestType {
	WEB_SERVER(0),
	CLIENT(1);
	
    private final int id;
    private FilmRequestType(int id) { this.id = id; }
    public int getValue() { return id; }
}
